<?php
require 'functions.php';

add_mapped_cols();

copy_files();
echo "Files has been copied and convert";

$start_memory = memory_get_usage();
$files = getFiles (); //1.65GB
$size = memory_get_usage() - $start_memory;
echo "\nLoaded files took $size BYTES in memory";

$files_size = $files['size'];
unset($files['size']);

if ($files_size == 0)
	die("\nERROR connect to local filesystem OR LibreOffice is running\n");

$result = read_head($files, $files_size); //1MB
if ($result != 0){
	echo "\nWARNING!!! Script added new cols, check this in 'Mapowanie' table\n";
	$message = "Powiadomienie sktyptu KAPT\nDodano nowe pole w tabeli 'mapowanie', w następnym uruchomieniu skrypu będzie automatycznie dodana do głównej tabeli. Zobacz ostatni wpis w tabeli 'mapowanie' aby przepisać nową kolumnę do istnijącej lub ją usunąć wpisując TRASH";
	mail('igor.galczak@agencjapracytworczej.pl', 'KAPT script - Warning', $message, 'Content-type: text/html; charset=utf-8');
	exit;
}

$result =read_legend($files, $files_size); //0.5MB
if ($result === false){
	echo "\nERROR!!! Some problem with read legends from file, check files structure\n";
	exit;
}

$result = just_do_it($files, $files_size); //7.7MB
if ($result === false){

}

echo create_emails_table();

exec("rm -r kapt  > /dev/null 2>&1");
echo "\nDONE\n";
?>