<?php 
class SQL{
	
	public function __construct($host, $user, $pass, $db){
		$this->connect = new mysqli($host, $user, $pass, $db);
		if(!$this->connect) {
			$this->connect->connect_error;
			return false;
		}
		$this->connect->set_charset('utf8');
		return true;
	}
	
	public function getError(){
		return $this->connect->error;
	}
	
	public function query($query){
		return $this->connect->query($query);
	}
	
	public function FetchAssoc($query){
		$result = $this->connect->query($query);
		if (!$result)
			return false;
		$arr = array();
		while($arr[] = $result->fetch_Assoc());
		unset($arr[count($arr)-1]);
		return $arr;
	}
	
	public function FetchAll($query){
		$result = $this->connect->query($query);
		if (!$result)
			return false;
		return $result->fetch_All();
	}
	
	public function Insert($table, $data){
		$fields = array_keys( $data );
		return $this->connect->query("INSERT INTO `$table`(".implode(",",$fields).") VALUES ('".implode("','", $data )."')");
	}
	
	public function MultiInsert($table, $data){
		if (!is_array($data[0]))
			return false;
			$fields = "`".implode("`,`",array_keys( $data[0] ))."`";
			$query = "INSERT INTO `$table`($fields) VALUES\n";
			$arr_q = array();
			foreach ($data as $row)
				$arr_q[] = "('".implode("','", $row )."')";
			$query .= implode(",\n",$arr_q);
			return $this->connect->query($query);
	}
	
	public function Update($table, $data, $where){
		$query = array();
		foreach ($data as $key=>$val){
			$query[] = "`$key`='$val'";
		}
		return $this->connect->query("UPDATE `$table` SET ".implode(', ',$query)." WHERE $where");
	}
	
	public function DescribeTable($table){
		$result = $this->connect->query("DESCRIBE $table");
		if(!$result)
			return false;
		$arr = array();
		while($arr[] = $result->fetch_Assoc());
		return $arr;
	}
	
	function ShowTables(){
		$result = $this->connect->query("SHOW TABLES");
		if(!$result)
			return false;
		$arr = array();
		while($temp = $result->fetch_Assoc()){
			foreach ($temp as $t){
				$arr[] = $t;
			}
		}
		return $arr;
	}
	
	public function addColumn($table, $col, $type='varchar(255)'){
		$sql = "ALTER TABLE $table ADD $col $type";
		return $this->connect->query($sql);
	}
	
	public function changeColumn($table, $col_old, $col_new, $type='varchar', $len=255){
		$sql = "ALTER TABLE $table CHANGE $col_old $col_new $type($len)";
		return $this->connect->query($sql);
	}
	
	public function removeColumn($table, $col){
		$sql = "ALTER TABLE $table DROP COLUMN $col";
		return $this->connect->query($sql);
	}
}
?>