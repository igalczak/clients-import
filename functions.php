<?php
require 'config.php';

function getFiles() {
	$files = array ();
	$i = 0;
	$size = 0;
	foreach ( glob ( "kapt/*" ) as $file ) {
		$objReader = new PHPExcel_Reader_Excel2007();
		$objReader->setReadDataOnly ( true );
		$file_name = basename ( $file );
		$files [$i] ['obj'] = $objReader->load ( $file );
		$files [$i] ['name'] = $file_name;
		$files [$i] ['size'] = filesize($file);
		$size += $files [$i] ['size'];
		$i ++;
	}
	$files['size'] = $size;
	return $files;
}

function copy_files(){
	global $pathes;
	if (!is_dir("before"))
		exec("mkdir before > /dev/null 2>&1");
	if (!is_dir("kapt"))
		exec("mkdir before > /dev/null 2>&1");
	exec("echo 'admin_pass' | sudo -S mount -t cifs -o username=igalczak,password='password_to_windows_disks',domain=APT //192.168.10.11/DYSKI /home/igalczak/Dyski > /dev/null 2>&1");
	exec("rm kapt/*  > /dev/null 2>&1");
	exec("rm before/*  > /dev/null 2>&1");

	foreach ($pathes as $path){
		if (!is_file($path))
			echo "File $path dont found\n";
		exec('cp "'.$path.'" before/ > /dev/null 2>&1');
	}

	exec("libreoffice --convert-to xlsx --outdir kapt before/* > /dev/null 2>&1");
	exec("rm -r before  > /dev/null 2>&1");
	exec("rm kapt/.* > /dev/null 2>&1");
}

function create_emails_table(){
	global $sql, $table_emails, $table_product;
	
	echo "\nCreating emails table\n";
	
	$sql->query ( "TRUNCATE `$table_emails`" );
	$query = implode("\n",file("union_query.txt"));
	$emails = $sql->FetchAssoc($query);
	
	foreach ($emails as &$row){
		$tmp = $sql->FetchAssoc("SELECT * FROM $table_product WHERE Produkt='".$row['product']."'")[0];
		$row['market'] = $tmp['Rynek'];
		$row['product_short_name'] = $tmp['Nazwa_skrucona'];
		$row['product_full_name'] = $tmp['Nazwa_pelna'];
	}
	
	$sql->MultiInsert($table_emails, $emails);


	return "Unique emails: ".count($emails)."\n";
}

function add_mapped_cols(){
	global $sql, $table_map, $table_main;
	
	$map = $sql->FetchAssoc("SELECT * FROM $table_map ORDER BY mapped ASC");
	
	$cols = array();
	$i=$j=0;
	foreach($map as $m)
		if(!in_array($m['mapped'], $cols)){
			$cols[] = $m['mapped'];
			$col = str_replace('-','_',$m['mapped']);
			$type = $m['type'];
			$query = "SHOW COLUMNS FROM $table_main WHERE FIELD='$col'";
			if(!$sql->FetchAssoc($query)[0] && $col!='' && $type!=''){
				if($sql->addColumn($table_main,$col, $type)){
					$i++;
				}
				else{
					echo $sql->getError()."\n";
					$j++;
				}
			}
	}
	
	return "Dodano $i kolumn do głównej tabeli\nBłędy - $j\n";
}

function read_head($files, $files_size) {
	global $sql, $table_map, $wykluczone;
	$start_memory = memory_get_usage();
	$added_cols = 0;
	$procent = 0;
	echo "\nReading headers\n";
	echo "$procent%\r";
	$done_size = 0;
	foreach ( $files as $key=>$file ) {
		$names = $file ['obj']->getSheetNames ();
		foreach ( $names as $key => $name )
			if (in_array ( strtolower ( $name ), $wykluczone [$file ['name']] ))
				unset ( $names [$key] );
		foreach ( $names as $name ) {
			$objWorksheet = $file ['obj']->getSheetByName ( $name );
			foreach ( $objWorksheet->getRowIterator () as $row ) {
				
				$cellIterator = $row->getCellIterator ();
				$cellIterator->setIterateOnlyExistingCells ( false );
				foreach ( $cellIterator as $cell ) {
					$real_val = str_replace ( array (
							'"',
							"'" 
					), '', $cell->getValue () );
					if (! $sql->FetchAssoc ( "SELECT * FROM $table_map WHERE header='$real_val'" ) [0] ['id']) {
						$insert = array (
								'header' => $real_val,
								'mapped' => $real_val,
								'type' => 'varchar(255)',
								'plik' => $file ['name'] 
						);
						$sql->Insert ( $table_map, $insert );
						$added_cols ++;
					} else {
						$plik = $sql->FetchAssoc ( "SELECT * FROM $table_map WHERE header='$real_val'" ) [0] ['plik'];
						if (! strpos ( $plik, $file ['name'] ))
							$sql->query ( "UPDATE `$table_map` SET `plik`='$plik ".$file['name']."' WHERE header='$real_val'" );
					}
				}
				break;
			}
		}
		$done_size += $file['size'];
		$procent = round(($done_size/$files_size)*100);
		echo "$procent%\r";
	}
	$size = memory_get_usage() - $start_memory;
	echo "\nMemory for update cols function: $size BYTES";
	return $added_cols;
}

function read_legend($files, $files_size) {
	global $sql, $table_product, $product_struct;
	$start_memory = memory_get_usage();
	$added_products = 0;
	$updated_products = 0;
	$procent = 0;
	echo "\nUpdating products\n";
	echo "$procent%\r";
	$done_size = 0;
	$products = $sql->FetchAssoc("SELECT * FROM $table_product");
	foreach ($products as $key=>$product){
		if ($product['Produkt'] == '')
			unset($products[$key]);
	}
	foreach ( $files as $file ) {
		$is_first_row = true;
		$names = $file ['obj']->getSheetNames ();
		$objWorksheet = false;
		foreach ($names as $name){
			if (strtolower($name) == 'legenda')
				$objWorksheet = $file ['obj']->getSheetByName ( $name );
		}
		if ($objWorksheet){
	
			foreach ( $objWorksheet->getRowIterator () as $row ) {
				$cellIterator = $row->getCellIterator ();
				$cellIterator->setIterateOnlyExistingCells ( false );
				$insert = array ();
				$cell_numer = 0;
				foreach ( $cellIterator as $cell ) {
					if ($cell_numer >= count($product_struct)) break;
					if ($is_first_row) break;
					$real_data = $cell->getCalculatedValue ();
					$real_data = str_replace ( array (
							'"',
							"'"
					), '', $real_data);
					if ($cell->getDataType () == 'n' && $real_data > 30000 && $real_data < 50000) {
						$real_data = ($real_data - 25569) * 86400;
						$real_data = date ( "Y-m-d", $real_data );
					}
					$insert [$product_struct [$cell_numer]] = $real_data;
					$cell_numer ++;
				}
				if (!$is_first_row){
					$exist = false;
					foreach ($products as $product){
						if ($product['Produkt'] == $insert['Produkt']){
							$exist = true;
							break;
						}
					}
					if ($exist){
						if ($sql->Update($table_product, $insert, "Produkt='".$insert['Produkt']."'"))
							$updated_products++;
						else
							echo "\n{read_legend function} UPDATE ERROR: ".$sql->getError();
					}
					elseif (strlen($insert['Produkt']) > 1 ){
						foreach ($insert as $key=>$val)
							if (!in_array($key,$product_struct))
								unset($insert[$key]);
								if ($sql->Insert($table_product, $insert))
									$added_products++;
									else
										echo "\n{read_legend function} INSERT ERROR: ".$sql->getError();
					}
				}
	
				$is_first_row = false;
			}
		}
		$done_size += $file['size'];
		$procent = round(($done_size/$files_size)*100);
		echo "$procent%\r";
	}
	$size = memory_get_usage() - $start_memory;
	echo "\nMemory for update products function: $size BYTES";
	return "\nDodano $added_products produktów\nZmieniono $updated_products produktów";
	
}

function just_do_it($files, $files_size) {
	global $sql, $table_map, $table_main, $table_logs, $wykluczone;
	$start_memory = memory_get_usage();
	
	$procent = 0;
	echo "\nAdding records to database\n";
	echo "$procent%\r";
	$done_size = 0;
	
	$ilosc_plikow = count($files);
	$wstawione_logi = 0;
	$blad_logow = 0;
	$rekordy = 0;
	
	$sql->query ( "TRUNCATE $table_main" );
	
	foreach ( $files as $file ) {
	
		$names = $file['obj']->getSheetNames ();
	
		foreach ( $names as $key => $name ) {
			if (in_array ( strtolower ( $name ), $wykluczone [$file['name']] )) {
				unset ( $names [$key] );
			}
		}
		foreach ($names as $name){
			$objWorksheet = $file['obj']->getSheetByName ( $name );
			$log = array ();
			$log ['inserted'] = 0;
			$log ['rows'] = - 1; // -1 bo zaczynamy od naglowka
			$log ['error_insert'] = '';
			$log ['duplicate'] = 0;
			$log ['error_count'] = 0;
			$temp = 0;
			$mapped = array ();
			foreach ( $objWorksheet->getRowIterator () as $row ) {
				$insert = array ();
				$log ['rows'] ++;
				$cellIterator = $row->getCellIterator ();
				$cellIterator->setIterateOnlyExistingCells ( false );
				$cell_numer = 0;
				$token = '';
				foreach ( $cellIterator as $cell ) {
					$real_val = str_replace ( array (
							'"',
							"'"
					), '', $cell->getValue () );
					if ($temp == 0) {
						$exist = $sql->FetchAssoc ( "SELECT * FROM $table_map WHERE header='$real_val'" ) [0];
						if ($exist) {
							$mapped [$cell_numer] ['pole'] = $exist ['mapped'];
							$mapped [$cell_numer] ['plik'] = $file ['name'];
						} else
							die ( "\nBRAK ODPOWIEDNIEJ KOLUMNY W TABELI, URUCHOM SKRYPT read_head.php NAJPIERW!!!\n" );
					} else {
						$real_data = $cell->getCalculatedValue ();
						$real_data = str_replace ( array (
								'"',
								"'"
						), '', $real_data );
						if ($cell->getDataType () == 'n' && $real_data > 30000 && $real_data < 50000) {
							$real_data = ($real_data - 25569) * 86400;
							$real_data = date ( "Y-m-d", $real_data );
						}
						$token .= $real_data;
						$insert [$mapped [$cell_numer] ['pole']] = $real_data;
					}
					$cell_numer ++;
				}
				$token = md5 ( $token );
				$insert ['Token'] = $token;
				$insert ['Created_at'] = date ( "Y-m-d H:i:s" );
				$insert ['PLIK_POCHODZENIA'] = $file ['name'];
					
				if (! $sql->FetchAssoc ( "SELECT Id FROM $table_main WHERE Token='$token'" ) [0] ['Id'] && $token != md5 ( '' )) {
					if ($sql->Insert ( $table_main, $insert )) {
						$rekordy ++;
						$log ['inserted'] ++;
					} else {
						$error = $sql->getError ();
						$log ['error_count'] ++;
						$log ['error_insert'] .= urlencode ( $error );
						echo "\nERROR: $error";
					}
				} else
					$log ['duplicate'] ++;
						
					$temp ++;
			}
	
			$log_insert = array (
					'Nazwa_pliku' => $file ['name'],
					'Data' => date ( "Y-m-d H:i:s" ),
					'Wszystkie_wiersze' => $log ['rows'],
					'Wstawione_wiersze' => $log ['inserted'],
					'Zduplikowane_wiersze' => $log ['duplicate'],
					'Ilosc_bledow' => $log ['error_count'],
					'Ostatni_blad' => $log ['error_insert']
			);
			$res = $sql->Insert ( $table_logs, $log_insert );
			if ($res)
				$wstawione_logi ++;
				else {
					echo "\n" . $sql->getError ();
					$blad_logow ++;
				}
		}
		$done_size += $file['size'];
		$procent = round(($done_size/$files_size)*100);
		echo "$procent%\r";
	}
	$query = "UPDATE `$table_main` SET `TRASH`=''";
	$sql->query ( $query );
	$size = memory_get_usage() - $start_memory;
	echo "\nMemory for main function: $size BYTES";
	return "\nZrobiono $ilosc_plikow plików\nDodano $rekordy rekordów\nWstawione Logi: $wstawione_logi\nBłędy w wstawioniu Logów: $blad_logow";

}

?>
