<?php 
error_reporting(E_ALL ^ E_NOTICE);
require_once('PHPExcel/Classes/PHPExcel.php');
require_once 'SQL.php';

$sql = new SQL('192.168.10.13', 'root', 'password', 'KAPT');
$table_map = 'Mapowanie';
$table_main = 'Klienci';
$table_logs = 'Logs';
$table_product = 'Produkty';
$table_emails = 'Klienci-emails';
$product_struct = array (
		"Produkt",
		"Nazwa_pelna",
		"Nazwa_skrucona",
		"Rynek",
		"Termin",
		"Miejscowosc"
);

/*
 * Wykluczone arkusze z danego pliku, nazwa pliku musi być dokładna,
 * nazwa arkusza - male litery
 */
$wykluczone=array(
		'KAPT-DDPCD.xlsx'=>array('warianty', 'kopia-all','legenda'),
		'KAPT-DDSCD.xlsx'=>array('warianty','legenda'),
		'KAPT-Konferencje.xlsx'=>array('legenda'),
		'KAPT-Pracownia-Nauczanki.xlsx'=>array('legenda terminy','legenda'),
		'KAPT-uczestnicy-szkolen.xlsx'=>array('legenda terminy','legenda'),
		'KAPT-GEODEZJA-prenumeratorzy.xlsx'=>array('darmowe wydania', 'warianty','legenda'),
		'KAPT-JADLOSPISY.xlsx'=>array('pokazowy jadłospis maile', 'kapt_jadlospisy_zr','legenda'),
		'KAPT-DROGI-prenumeratorzy.xlsx'=>array('darmowe wydania', 'warianty', 'etykiety', 'etykiety podwójne', 'etykiety darmówki', 'fv-prof-01-2014','legenda'),
		'KAPT-EDU-prenumeratorzy.xlsx'=>array('darmowe wydania', 'warianty', 'renewal','legenda'),
		'KAPT-MwU-prenumeratorzy.xlsx'=>array('legenda'),
		'KAPT-IN.xlsx'=>array('maile', 'darmowe wydania', 'warianty','legenda'),
		'KAPT-Wysportowane-dzieciaki.xlsx'=>array('legenda'),
		'KAPT-Garnek-Inspiracji.xlsx'=>array('legenda'),
		'KAPT-GetCreative.xlsx'=>array('legenda', 'warianty', 'darmowe wydania', 'maile'),
);
$ILE_PLIKOW = 14;
$ILE_ARKUSZY = array(
		'KAPT-DDPCD.xlsx'=>7,
		'KAPT-DDSCD.xlsx'=>6,
		'KAPT-Konferencje.xlsx'=>5,
		'KAPT-Pracownia-Nauczanki.xlsx'=>8,
		'KAPT-uczestnicy-szkolen.xlsx'=>24,
		'KAPT-GEODEZJA-prenumeratorzy.xlsx'=>3,
		'KAPT-JADLOSPISY.xlsx'=>8,
		'KAPT-DROGI-prenumeratorzy.xlsx'=>7,
		'KAPT-EDU-prenumeratorzy.xlsx'=>4,
		'KAPT-IN.xlsx'=>4,
		'KAPT-Wysportowane-dzieciaki.xlsx'=>1,
		'KAPT-Garnek-Inspiracji.xlsx'=>1,
		'KAPT-MwU-prenumeratorzy.xlsx'=>2,
		'KAPT-GetCreative.xlsx'=>5,
);

$pathes = array(
		'/home/igalczak/Dyski/CustomerService/Klienci - seminaria/KAPT-Konferencje.xlsx',
		'/home/igalczak/Dyski/CustomerService/Klienci - seminaria/KAPT-Pracownia-Nauczanki.xlsx',
		'/home/igalczak/Dyski/CustomerService/Klienci - seminaria/KAPT-uczestnicy-szkolen.xlsx',
		'/home/igalczak/Dyski/CustomerService/Klienci - publikacje/MARKETING W URZĘDZIE/KAPT-MwU-prenumeratorzy.xlsx',
		'/home/igalczak/Dyski/CustomerService/Klienci - publikacje/GEODEZJA/KAPT-GEODEZJA-prenumeratorzy.xlsx',
		'/home/igalczak/Dyski/CustomerService/Klienci - publikacje/DROGI/KAPT-DROGI-prenumeratorzy.xlsx',
		'/home/igalczak/Dyski/CustomerService/Klienci - publikacje/EDU/KAPT-DDPCD.xlsx',
		'/home/igalczak/Dyski/CustomerService/Klienci - publikacje/EDU/KAPT-DDSCD.xlsx',
		'/home/igalczak/Dyski/CustomerService/Klienci - publikacje/EDU/KAPT-EDU-prenumeratorzy.xlsx',
		'/home/igalczak/Dyski/CustomerService/Klienci - publikacje/EDU/KAPT-Garnek-Inspiracji.xlsx',
		'/home/igalczak/Dyski/CustomerService/Klienci - publikacje/EDU/KAPT-IN.xlsx',
		'/home/igalczak/Dyski/CustomerService/Klienci - publikacje/EDU/KAPT-JADLOSPISY.xlsx',
		'/home/igalczak/Dyski/CustomerService/Klienci - publikacje/EDU/KAPT-Wysportowane-dzieciaki.xlsx',
		'/home/igalczak/Dyski/CustomerService/Klienci - publikacje/EDU/KAPT-GetCreative.xlsx',
);

function getmicrotime()
{
	return array_sum(explode(' ', microtime()));
}
?>
